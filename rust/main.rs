extern mod extra;

use std::io::buffered::BufferedStream;
use std::io::net::addrinfo::get_host_addresses;
use std::io::net::ip::{IpAddr, SocketAddr};
use std::io::net::tcp::TcpStream;
use std::os::args;

use extra::json;
use extra::json::{Json, ToJson, Number, String, Boolean, List, Object, Null};
use extra::treemap::TreeMap;
use std::f64;
use extra::serialize::Decodable;

trait Protocol {
  fn msg_type(&self) -> ~str;
  fn json_data(&self) -> Json;
}

struct Msg {
  msgType: ~str,
  data: Json
}

impl Protocol for Msg {
  fn msg_type(&self) -> ~str { self.msgType.clone() }
  fn json_data(&self) -> Json { self.data.clone() }
}

struct JoinMsg {
  name: ~str,
  key: ~str
}

#[deriving(Decodable)]
struct YourCar {
  name: ~str,
  color: ~str
}

impl Protocol for JoinMsg {
  fn msg_type(&self) -> ~str { ~"join" }
  fn json_data(&self) -> Json {
    let mut m = TreeMap::new();
    m.insert(~"name", self.name.to_json());
    m.insert(~"key", self.key.to_json());
    return json::Object(~m);
  }
}

struct JoinRaceMsg {
  botId: JoinMsg,
  trackName: ~str,
  //password: ~str,
  carCount: ~int
}

impl Protocol for JoinRaceMsg {
  fn msg_type(&self) -> ~str { ~"joinRace" }
  fn json_data(&self) -> Json {
    let mut m = TreeMap::new();
    m.insert(~"botId", self.botId.json_data());
    m.insert(~"trackName", self.trackName.to_json());
    m.insert(~"carCount", self.carCount.to_json());
    //m.insert(~"password", self.password.to_json());
    return json::Object(~m);
  }
}

struct ThrottleMsg {
  value: f64
}

impl Protocol for ThrottleMsg {
  fn msg_type(&self) -> ~str { ~"throttle" }
  fn json_data(&self) -> Json { json::Number(self.value) }
}

struct SwitchMsg {
  value: ~str
}

impl Protocol for SwitchMsg {
  fn msg_type(&self) -> ~str { ~"switchLane" }
  fn json_data(&self) -> Json { self.value.to_json() }
}

struct TurboMsg {
  value: ~str
}

impl Protocol for TurboMsg {
  fn msg_type(&self) -> ~str { ~"turbo" }
  fn json_data(&self) -> Json { self.value.to_json() }
}

struct Piece {
  length: Option<f64>,
  switch: Option<bool>,
  radius: Option<f64>,
  angle: Option<f64>
}

impl Piece {
  fn bend_length(&self) -> f64 {
    let pi = f64::consts::PI;
    match self.angle {
      Some(_) => {
        (self.angle.unwrap() * pi * self.radius.unwrap()) / 180.0
      },
      None => 0.0
    }
  }

  fn is_switch(&self) -> bool {
    match self.switch {
      Some(_) => true,
      None => false
    }
  }

  fn is_bend(&self) -> bool {
    match self.angle {
      Some(_) => true,
      None => false
    }
  }
}

#[deriving(Decodable)]
struct CarPosition {
    id: ~CarId,
    angle: ~f64,
    piecePosition: ~PiecePosition,
}

#[deriving(Decodable)]
struct CarId {
  name: ~str,
  color: ~str
}

#[deriving(Decodable)]
struct PiecePosition {
    pieceIndex: uint,
    inPieceDistance: ~f64,
    lane: Lane,
    lap: ~int
}

#[deriving(Decodable)]
struct Lane {
  startLaneIndex: ~int,
  endLaneIndex: ~int
}

#[deriving(Decodable)]
struct LapFinished {
  car: ~CarId,
  lapTime: ~LapTime,
  raceTime: ~RaceTime,
  ranking: ~Ranking
}

#[deriving(Decodable)]
struct RaceTime {
  laps: ~int,
  ticks: ~int,
  millis: ~int
}

#[deriving(Decodable)]
struct LapTime {
  lap: ~int,
  ticks: ~int,
  millis: ~int
}

#[deriving(Decodable)]
struct Ranking {
  overall: ~int,
  fastestLap: ~int
}

fn write_msg<T: Protocol>(msg: &T, stream: &mut BufferedStream<TcpStream>) {
  let mut json = TreeMap::new();
  json.insert(~"msgType", msg.msg_type().to_json());
  json.insert(~"data", msg.json_data());

  write_json(&json::Object(~json), stream);
}

fn write_json(json: &Json, stream: &mut BufferedStream<TcpStream>) {
  json.to_writer(stream);
  stream.write_char('\n');
  stream.flush();
}

fn parse_msg(json: &~json::Object) -> Option<Msg> {
  match json.find(&~"msgType") {
    Some(&json::String(ref msgType)) => {
      let data = json.find(&~"data").unwrap_or(&json::Null);
      Some(Msg {
        msgType: msgType.clone(),
        data: data.clone()
      })
    }
    _ => None
  }
}

fn handle_msg(msg: ~Msg, stream: &mut BufferedStream<TcpStream>) {
  match msg.msgType {
    ~"join" => println!("Joined"),
    ~"gameEnd" => println!("Game end"),
    ~"gameStart" => println!("Game start"),
    ~"error" => println!("ERROR: {}", msg.data.to_str()),
    _ => println!("Got {}", msg.msgType)
  }
  write_msg(&Msg {
    msgType: ~"ping",
    data: json::Null
  }, stream);
}

fn init_game(data: &Json) -> ~[Piece] {
   let mut piecesVector = ~[];
   match data {
    &Object(ref root) => {
      match root.find(&~"race") {
        Some(&Object(ref race)) => {
          match race.find(&~"track") {
            Some(&Object(ref track)) => {
              match track.find(&~"pieces") {
                Some(&List(ref pieces)) => {
                  for piece in pieces.iter() {
                    match piece {
                      &Object(ref pieceValue) => {
                      piecesVector.push(Piece {
                        length: match pieceValue.find(&~"length") {
                            Some(&Number(ref value)) => Some(*value),
                            _ => None
                          },
                        switch: match pieceValue.find(&~"switch") {
                            Some(&Boolean(ref value)) => Some(*value),
                            _ => None
                          },
                        radius: match pieceValue.find(&~"radius") {
                            Some(&Number(ref value)) => Some(*value),
                            _ => None
                          },
                        angle: match pieceValue.find(&~"angle") {
                            Some(&Number(ref value)) => Some(*value),
                            _ => None
                          }
                      })
                      }
                      _ => println!("piece is not a JSON object")
                    }
                  }
                  println!("hooray, pieces found!")
                },
                _ => println!("init: failed at pieces")
              }
            },
            _ => println!("init: failed at track")
          }
        },
        _ => println!("init: failed at race")
      }
    },
    _ => println!("init: failed root")
  }
  println!("Init game data: {:s}", data.to_str());
  piecesVector
}

fn start(config: Config) {
  let Config { server, name, key } = config;

  println!("Attempting to connect to {:s}", server.to_str());
  let mut stream = BufferedStream::new(
    TcpStream::connect(server).expect("Failed to connect"));
  println!("Connected");

  let joinMsg = JoinMsg {
    name: name,
    key: key
  };

  /*
  let joinRaceMsg = JoinRaceMsg {
    botId: joinMsg,
    trackName: ~"france",
    //password: ~"password",
    carCount: ~1,
  };
  write_msg(&joinRaceMsg, &mut stream);
  */

  write_msg(&joinMsg, &mut stream);

  let mut pieces = ~[];
  let mut gameTick: f64 = 0.0;
  let mut inPieceDistance: f64 = 0.0;
  let mut base_max_throttle = 1.0;
  let mut base_min_throttle = 0.25;
  let too_fast = 6.0;
  let way_too_fast = 9.0;
  let mut has_crashed_this_lap = false;
  let mut crash_max_throttle_penalty = 0.05;
  let mut max_angle_this_lap = 0.0;
  let mut this_car = YourCar {name: ~"default name", color: ~"default color"};
  let mut turbo_available = false;
  loop {
    match stream.read_line() {
      None => break,
      Some(line) => match json::from_str(line) {
        Ok(json::Object(ref v)) => {
          match parse_msg(v) {
            None => println("Invalid JSON data"),
            Some(msg) => {
              match  msg.msgType {
                ~"turboAvailable" => {
                  println!("Turbo available!");
                  turbo_available = true;
                },
                ~"lapFinished" => {
                  let mut decoder = json::Decoder::new(msg.data);
                  let lap_finished: LapFinished = Decodable::decode(&mut decoder);
                  if lap_finished.car.color == this_car.color {
                    println!("Lap finished");
                    if !has_crashed_this_lap && max_angle_this_lap < 50.0 {
                      base_max_throttle += 0.035;
                      println!("Max angle: {}, base_max_throttle now {}", max_angle_this_lap, base_max_throttle);
                    } else {
                      println!("Max angle: {}", max_angle_this_lap);
                    }
                    if max_angle_this_lap < 25.0 {
                      base_min_throttle *= 1.2;
                    } else if max_angle_this_lap < 35.0 {
                      base_min_throttle *= 1.1;
                    }
                    has_crashed_this_lap = false;
                    max_angle_this_lap = 0.0;
                  }

                  write_msg(&Msg {
                    msgType: ~"ping",
                    data: json::Null
                  }, &mut stream);
                },
                ~"gameInit" => {
                  pieces = init_game(&msg.data);
                  write_msg(&Msg {
                    msgType: ~"ping",
                    data: json::Null
                  }, &mut stream);
                },
                ~"yourCar" => {
                  let mut decoder = json::Decoder::new(msg.data);
                  this_car = Decodable::decode(&mut decoder);
                  println!("YourCar message received. My name is {} and I'm {}.", this_car.name, this_car.color);
                  write_msg(&Msg {
                    msgType: ~"ping",
                    data: json::Null
                  }, &mut stream);
                },
                ~"crash" => {
                  let mut decoder = json::Decoder::new(msg.data);
                  let crashed_car: CarId = Decodable::decode(&mut decoder);
                  if crashed_car.color == this_car.color {
                    base_max_throttle -= crash_max_throttle_penalty;
                    crash_max_throttle_penalty *= 1.1;
                    println!("Got crash. base_max_throttle now {}", base_max_throttle);
                    has_crashed_this_lap = true;
                  } else {
                    println!("Somebody else crashed.");
                  }
                  write_msg(&Msg {
                    msgType: ~"ping",
                    data: json::Null
                  }, &mut stream);
                }
                ~"carPositions" => {
                  let newTick = match v.find(&~"gameTick") {
                    Some(&Number(ref value)) => *value,
                    _ => {
                      println!("gameTick parsing failed");
                      gameTick + 1.0
                    }
                  };
                  let mut decoder = json::Decoder::new(msg.data);
                  let positions: ~[CarPosition] = Decodable::decode(&mut decoder);
                  let my_position = find_my_position(positions, this_car.color.clone());
                  let ~newInPieceDistance = my_position.piecePosition.inPieceDistance;

                  let mut speed = -1.0;
                  if newInPieceDistance > inPieceDistance {
                    speed = (newInPieceDistance-inPieceDistance);
                  }
                  max_angle_this_lap = std::num::max(max_angle_this_lap, std::num::abs(*my_position.angle));
                  println!("{} {} {}", newTick, my_position.angle.to_str(), speed.to_str());

                  gameTick = newTick;
                  inPieceDistance = newInPieceDistance;

                  match gameTick as int % 10 {
                    5 => { match find_direction_for_next_switch(&pieces, my_position.piecePosition.pieceIndex) {
                      Some(string) => write_msg(&SwitchMsg {
                        value: string
                      }, &mut stream),
                      _ => {
                        write_msg(&Msg {
                          msgType: ~"ping",
                          data: json::Null
                        }, &mut stream);
                      }
                    }},
                    _ => {
                      let mut new_throttle:f64 = base_max_throttle;

                      let (distance, difficulty) = find_next_bend_distance_and_difficulty(&pieces, my_position.piecePosition.pieceIndex);
                      let speed_adjusted_difficulty = if speed > too_fast {
                          difficulty + 1.0
                        } else {
                          difficulty
                        };
                      if distance <= speed_adjusted_difficulty * 100.0 {
                        new_throttle = base_max_throttle - std::num::pow(difficulty, 2.0) / 10.0;
                      }

                      if speed > way_too_fast {
                        new_throttle = 0.0;
                      }

                      if speed_adjusted_difficulty > 2.0 && distance <= 100.0 && speed > too_fast {
                        new_throttle *= 0.8;
                      }

                      new_throttle = std::num::min(new_throttle, 1.0);
                      new_throttle = std::num::max(new_throttle, base_min_throttle);
                      
                      println!("Throttle: {}, difficulty: {}, distance: {}", new_throttle, speed_adjusted_difficulty, distance);
                      
                      if turbo_available && distance > 400.0 && difficulty <= 2.0 && std::num::abs(*my_position.angle) < 35.0 {
                        println!("====== Activating turbo!! ======");
                        turbo_available = false;
                        write_msg(&TurboMsg {
                          value: ~"Speed is in excess of R17!"
                        }, &mut stream);
                      } else {
                        write_msg(&ThrottleMsg {
                          value: new_throttle
                          // steady throttle values that should allow the car to survive different tracks:
                          // keimola: 0.65
                          // germany: 0.465
                          // usa: 0.96
                          // france: 0.55
                        }, &mut stream);
                      }
                    }
                  }
                },
                _ => handle_msg(~msg, &mut stream)
              }
            }
          }
        },
        Ok(_) => println("Invalid JSON data: expected an object"),
        Err(msg) => println(msg.to_str())
      }
    }
  }
  println!("Disconnected")
}

fn find_my_position<'a>(positions: &'a[CarPosition], color: ~str) -> &'a CarPosition {
  for position in positions.iter() {
    if position.id.color == color {
      return position;
    }
  }
  println!("Failed miserably.");
  return &'a positions[0];
}

fn find_next_bend_distance_and_difficulty(pieces: &~[Piece], currentPiece: uint) -> (f64, f64) {
  let mut distance = 0.0;
  let mut difficulty = 0.0;
  let mut i = 0;
  while i < pieces.len() {
    let index = (currentPiece + i) % pieces.len();
    let piece = pieces[index];
    if !piece.is_bend() {
      distance += pieces[index].length.unwrap();
    } else {
      let mut j = 0;
      while j < pieces.len() {
        let index2 = (index + j) % pieces.len();
        let piece2 = pieces[index2];
        if !piece2.is_bend() {
          break;
        }
        let diff2 = if piece2.radius.unwrap() < 60.0 { 3.0 }
                    else if piece2.radius.unwrap() < 120.0 { 2.0 }
                    else { 1.0 };
        difficulty = std::num::max(difficulty, diff2);
        j += 1;
      }
      break;
    }
    i += 1;
  }
  return (distance, difficulty);
}

fn find_direction_for_next_switch(pieces: &~[Piece], currentPiece: uint) -> Option<~str> {
  let mut i = 0;
  while i < pieces.len() {
    let index = (currentPiece + i) % pieces.len();
    if pieces[index].is_switch() {
      let mut j = 1;
      let mut bend_length = 0.0;
      while j < pieces.len() {
        let index2 = (index + j) % pieces.len();
        bend_length += pieces[index2].bend_length();
        if pieces[index2].is_switch() {
          return
            if bend_length < 0.0 { Some(~"Left") }
            else if bend_length > 0.0 { Some(~"Right") }
            else { None };
        }
        j += 1;
      }
    }
    i += 1;
  }
  return None;
}

struct Config {
  server: SocketAddr,
  name: ~str,
  key: ~str
}

fn resolve_first_ip(host: &str) -> Option<IpAddr> {
  match get_host_addresses(host) {
    Some([ip, ..]) => Some(ip),
    _ => None
  }
}

fn read_config() -> Option<Config> {
  let args = args();
  match args {
    [_, host, port_str, name, key] => {
      let ip = resolve_first_ip(host).expect("Could not resolve host");
      let port = from_str::<u16>(port_str).expect("Invalid port number");
      return Some(Config {
        server: SocketAddr { ip: ip, port: port },
        name: name,
        key: key
      });
    },
    _ => None
  }
}

fn main() {
  match read_config() {
    None => println("Usage: ./run <host> <port> <botname> <botkey>"),
    Some(config) => start(config)
  }
}
